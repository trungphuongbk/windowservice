@ECHO OFF

REM The following directory is for .NET 4.0
set DOTNETFX2=%SystemRoot%\Microsoft.NET\Framework\v4.0.30319
set PATH=%PATH%;%DOTNETFX2%


echo Installing IEPPAMS Win Service...
echo ---------------------------------------------------
echo Input action: De Install Service - i, De UnInstall Service - u

set /p input=""

if "%input%" == "i" (
	C:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil "%~dp0WindowsService1.exe"
	echo ---------------------------------------------------
	echo Start window service ...
	net start "DemoService"
)

if "%input%" == "u" (
	echo Uninstall window service ...
	C:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil /u "%~dp0WindowsService1.exe"
	echo ---------------------------------------------------
)

pause