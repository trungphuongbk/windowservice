﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindowsService1
{
    public partial class DemoService : ServiceBase
    {

        public DemoService()
        {
            InitializeComponent();
        }

        static void run()

        {
            //while (true)
            {
                StreamWriter str = new StreamWriter(@"D:\\ContinuosLoop.txt", true);
                str.WriteLine("Visit DotNetFunda Regularly");
                str.Close();
            }
        }

        Thread thr = new Thread(new ThreadStart(run));
        protected override void OnStart(string[] args)
        {
            StreamWriter str = new StreamWriter(@"D:\\Log.txt", true);
            str.WriteLine("Service started on : " + DateTime.Now.ToString());
            str.Close();
            thr.Start();
        }

        protected override void OnStop()
        {

            StreamWriter str = new StreamWriter(@"D:\\Log.txt", true);
            str.WriteLine("Service stoped on : " + DateTime.Now.ToString());
            str.Close();
            thr.Abort();
        }
    }
}
